# dealiiStrainEnergy
To compute strain energy density based on open-source FEM deal.II library

## 1. Setup:
#### Installation
- FEM library: **deal.II** - **candi** package (Compile & Install): https://github.com/dealii/candi
- Visualization: ParaView

**TODO**:
- Installation commands for compilation
```bash
echo "TODO"
cmake XXX build_folder
```
- Try to remove all binaries and out them into gitignore (`a.out`, ``). Note that folders are also possible in gitignore with `build_folder/*`. Useful e.g. for ignoring the whole `build` folder.

#### Essential files
- main_LLZO.cc
- CMakeLists.txt
- FEM_LLZO.h
- FEM_LLZO_spatial.h

## 2. Strong form: Displacement vectorial unknown problem
```math
\begin{aligned}
    \nabla\cdot\mathbb{C}^{(\star)}\nabla_{s}\textbf{u} + \rho\textbf{b} = \textbf{0}
\end{aligned}
```
where $`\mathbb{C}^{(\star)}`$ has the following forms:
```math
\begin{aligned}
    \mathbb{C}_{ijkl}^{d^{R} \circ d^{E}}
                        &\triangleq \lambda\ \textbf{1} \otimes \textbf{1} + 2 \mu \ \mathbb{I} \\
                        &
                        + \alpha^{R} \left( \textbf{M}^{R}\otimes\textbf{1}
		                + \textbf{1}\otimes\textbf{M}^{R}\right)
		                + \delta^{R} \ \mathbb{I}_{\textbf{d}^{R}}
		                + \beta^{R} \textbf{M}^{R} \otimes \textbf{M}^{R} \\
                        &
                        + \alpha^{E} \left( \textbf{M}^{E}\otimes\textbf{1}
		                + \textbf{1}\otimes\textbf{M}^{E}\right)
		                + \delta^{E} \ \mathbb{I}_{\textbf{d}^{E}}
		                + \beta^{E} \textbf{M}^{E} \otimes \textbf{M}^{E} \\
                        &
                        + \gamma^{E} \left( \textbf{M}^{E} \otimes \textbf{M}^{R}
                                          + \textbf{M}^{R} \otimes \textbf{M}^{E}  \right)
\end{aligned}
```

| Material parameter           |        #1st        |      #2nd      |    #3rd    |   #4th    |    #5th    |    #6th    |
| ---------------------------- | :----------------: | :------------: | :--------: | :-------: | :--------: | :--------: |
| Laboratory measurement       | $`\lambda(E,\nu)`$ | $`\mu(E,\nu)`$ |            |           |            |            |
| Directional modelling effect |                    |                | $`\alpha`$ | $`\beta`$ | $`\delta`$ | $`\gamma`$ |
| Real material parameter      | $`\lambda(E,\nu)`$ | $`\mu(E,\nu)`$ | $`\alpha`$ | $`\beta`$ | $`\delta`$ | $`\gamma`$ |

## 3. Visualized displacement solution in 2D
- **Dirichlet** BC (most left point: most blue)
- **Neumann** BC (red zone, pull up at upper part and pull down at lower part)
> $ paraview

 |         12 elements         |         48 elements         |        192 elements         |
 | :-------------------------: | :-------------------------: | :-------------------------: |
 | ![pic](./Gallery/iso_0.png) | ![pic](./Gallery/iso_1.png) | ![pic](./Gallery/iso_2.png) |

## 4. Strain energy density $`\mathcal{E}`$ versus $`(E,\nu)`$

#### Full range of strain energy density $`\mathcal{E}`$ versus $`(E,\nu)`$ w.r.t. number of elements

 |                             12 elements                              |                             48 elements                              |                             192 elements                              |
 | :------------------------------------------------------------------: | :------------------------------------------------------------------: | :-------------------------------------------------------------------: |
 |                     ![pic](./Gallery/iso_0.png)                      |                     ![pic](./Gallery/iso_1.png)                      |                      ![pic](./Gallery/iso_2.png)                      |
 | ![pic](./Gallery/LowHighload_FullrangeEnu_strainenergy_LLZO_12e.png) | ![pic](./Gallery/LowHighload_FullrangeEnu_strainenergy_LLZO_48e.png) | ![pic](./Gallery/LowHighload_FullrangeEnu_strainenergy_LLZO_192e.png) |
 |                     1705 seconds <br/>≈ 28.4 min                     |                    16169 seconds <br/>≈ 4.5 hours                    |                    813669 seconds <br/>≈ 9.4 days                     |

#### Case of 12 elements: $`\mathcal{E}`$ of no-doped and doped LLZO
![pic](./Gallery/iso_0.png)

| Cases        |                   $`(E,\nu)`$-Low load                    |                   $`(E,\nu)`$-High load                    |
| ------------ | :-------------------------------------------------------: | :--------------------------------------------------------: |
| LLZO-noDoped |  ![pic](./Gallery/Lowload_noDoped_strainenergy_LLZO.png)  |  ![pic](./Gallery/Highload_noDoped_strainenergy_LLZO.png)  |
| 60Ta-LLZO    | ![pic](./Gallery/Lowload_60TaDoped_strainenergy_LLZO.png) | ![pic](./Gallery/Highload_60TaDoped_strainenergy_LLZO.png) |
| 20Ta-LLZO    | ![pic](./Gallery/Lowload_20TaDoped_strainenergy_LLZO.png) | ![pic](./Gallery/Highload_20TaDoped_strainenergy_LLZO.png) |
| 36Al-LLZO    | ![pic](./Gallery/Lowload_36AlDoped_strainenergy_LLZO.png) | ![pic](./Gallery/Highload_36AlDoped_strainenergy_LLZO.png) |
| 20Al-LLZO    | ![pic](./Gallery/Lowload_20AlDoped_strainenergy_LLZO.png) | ![pic](./Gallery/Highload_20AlDoped_strainenergy_LLZO.png) |
| Full range   | ![pic](./Gallery/Lowload_Fullrange_strainenergy_LLZO.png) | ![pic](./Gallery/Highload_Fullrange_strainenergy_LLZO.png) |

<!--- ![pic](./Gallery/LowHighload_FullrangeE_strainenergy_LLZO.png) -->


#### Case of 48 elements: $`\mathcal{E}`$ of no-doped and doped LLZO
![pic](./Gallery/iso_1.png)

| Cases        | $`(E,\nu)`$-Low load | $`(E,\nu)`$-High load |
| ------------ | :------------------: | :-------------------: |
| LLZO-noDoped |       1959 sec       |       2001 sec        |
| 60Ta-LLZO    |       1998 sec       |       2048 sec        |
| 20Ta-LLZO    |       2019 sec       |       2038 sec        |
| 36Al-LLZO    |       1975 sec       |       1972 sec        |
| 20Al-LLZO    |       1953 sec       |       1966 sec        |
| Full range   |      12172 sec       |       11933 sec       |

| Cases        |                       $`(E,\nu)`$-Low load                        |                       $`(E,\nu)`$-High load                        |
| ------------ | :---------------------------------------------------------------: | :----------------------------------------------------------------: |
| LLZO-noDoped |  ![pic](./Gallery/48e/Lowload_noDoped_strainenergy_LLZO_48e.png)  |  ![pic](./Gallery/48e/Highload_noDoped_strainenergy_LLZO_48e.png)  |
| 60Ta-LLZO    | ![pic](./Gallery/48e/Lowload_60TaDoped_strainenergy_LLZO_48e.png) | ![pic](./Gallery/48e/Highload_60TaDoped_strainenergy_LLZO_48e.png) |
| 20Ta-LLZO    | ![pic](./Gallery/48e/Lowload_20TaDoped_strainenergy_LLZO_48e.png) | ![pic](./Gallery/48e/Highload_20TaDoped_strainenergy_LLZO_48e.png) |
| 36Al-LLZO    | ![pic](./Gallery/48e/Lowload_36AlDoped_strainenergy_LLZO_48e.png) | ![pic](./Gallery/48e/Highload_36AlDoped_strainenergy_LLZO_48e.png) |
| 20Al-LLZO    | ![pic](./Gallery/48e/Lowload_20AlDoped_strainenergy_LLZO_48e.png) | ![pic](./Gallery/48e/Highload_20AlDoped_strainenergy_LLZO_48e.png) |
| Full range   | ![pic](./Gallery/48e/Lowload_Fullrange_strainenergy_LLZO_48e.png) | ![pic](./Gallery/48e/Highload_Fullrange_strainenergy_LLZO_48e.png) |
| Statistics   |  ![pic](./Gallery/48e/statistics/Lowload_5cases_LLZO_48e_st.png)  |  ![pic](./Gallery/48e/statistics/Highload_5cases_LLZO_48e_st.png)  |

![pic](./Gallery/48e/statistics/Highlowload_10cases_LLZO_48e_st_cl.png)


| Cases             |                              $`(E,\nu)`$-Low load                              |                              $`(E,\nu)`$-High load                              |
| ----------------- | :----------------------------------------------------------------------------: | :-----------------------------------------------------------------------------: |
| noDoped-60Ta-LLZO | ![pic](./Gallery/48e_fAl_HLLL/Lowload_60TaDoped_strainenergy_LLZO_48e_fAl.png) | ![pic](./Gallery/48e_fAl_HLLL/Highload_60TaDoped_strainenergy_LLZO_48e_fAl.png) |
| noDoped-20Ta-LLZO | ![pic](./Gallery/48e_fAl_HLLL/Lowload_20TaDoped_strainenergy_LLZO_48e_fAl.png) | ![pic](./Gallery/48e_fAl_HLLL/Highload_20TaDoped_strainenergy_LLZO_48e_fAl.png) |
| noDoped-36Al-LLZO | ![pic](./Gallery/48e_fAl_HLLL/Lowload_36AlDoped_strainenergy_LLZO_48e_fAl.png) | ![pic](./Gallery/48e_fAl_HLLL/Highload_36AlDoped_strainenergy_LLZO_48e_fAl.png) |
| noDoped-20Al-LLZO | ![pic](./Gallery/48e_fAl_HLLL/Lowload_20AlDoped_strainenergy_LLZO_48e_fAl.png) | ![pic](./Gallery/48e_fAl_HLLL/Highload_20AlDoped_strainenergy_LLZO_48e_fAl.png) |

![pic](./Gallery/48e_fAl_HLLL/statistics/Highlowload_10cases_LLZO_48e_st_cl_fAl.png)

![pic](./Gallery/48e_fAl_HLLL/statistics/Highlowload_10cases_LLZO_48e_st_cl_fAl_18.png)

![pic](./Gallery/case152.png)

#### Case of 192 elements: $`\mathcal{E}`$ of no-doped and doped LLZO
![pic](./Gallery/iso_2.png)

| Cases        | $`(E,\nu)`$-Low load | $`(E,\nu)`$-High load |
| ------------ | :------------------: | :-------------------: |
| LLZO-noDoped |      98268 sec       |       98094 sec       |
| 60Ta-LLZO    |      98431 sec       |       99860 sec       |
| 20Ta-LLZO    |          x           |           x           |
| 36Al-LLZO    |          x           |           x           |
| 20Al-LLZO    |          x           |           x           |
| Full range   |          x           |           x           |

## 5. Useful notes for generating figures from gnuplot

#### To plot the numbering of grid: adjustable (autoscale possible) --> recommended
> $ gnuplot
> $ load "matrix_sparsity_pattern_numbering.gpl"

#### To plot the numbering of grid: non-adjustable (autoscale not possible) --> not recommended
> $ gnuplot -p "matrix_sparsity_pattern_numbering.gpl"

#### To plot the pattern of matrix:
> $ gnuplot
> $ set style data points
> $ plot "matrix_sparsity_pattern.gpl" using 1:2 notitle
> $ set xrange [0:114]; replot
> $ set yrange [-114:0]; replot
> $ set xtics 19; replot
> $ set ytics 19; replot
> $ set xtics add("113" 113); replot
> $ set ytics add("-113" -113); replot

#### Discretization: Conventional grid numbering and its sparsity pattern

| Refinement                            |        Grid and Numbering        |          Sparsity pattern           |
| ------------------------------------- | :------------------------------: | :---------------------------------: |
| 12 bi-linear elements, <br/>34 dofs   | ![pic](./Gallery/grid_0_con.png) | ![pic](./Gallery/pattern_0_con.png) |
| 48 bi-linear elements, <br/>114 dofs  | ![pic](./Gallery/grid_1_con.png) | ![pic](./Gallery/pattern_1_con.png) |
| 192 bi-linear elements, <br/>418 dofs | ![pic](./Gallery/grid_2_con.png) | ![pic](./Gallery/pattern_2_con.png) |

#### Discretization: Random grid numbering and its sparsity pattern

| Refinement                            |        Grid and Numbering        |          Sparsity pattern           |
| ------------------------------------- | :------------------------------: | :---------------------------------: |
| 12 bi-linear elements, <br/>34 dofs   | ![pic](./Gallery/grid_0_ran.png) | ![pic](./Gallery/pattern_0_ran.png) |
| 48 bi-linear elements, <br/>114 dofs  | ![pic](./Gallery/grid_1_ran.png) | ![pic](./Gallery/pattern_1_ran.png) |
| 192 bi-linear elements, <br/>418 dofs | ![pic](./Gallery/grid_2_ran.png) | ![pic](./Gallery/pattern_2_ran.png) |

#### Discretization: Cuthill-Mckee grid numbering and its sparsity pattern

| Refinement                            |       Grid and Numbering        |          Sparsity pattern          |
| ------------------------------------- | :-----------------------------: | :--------------------------------: |
| 12 bi-linear elements, <br/>34 dofs   | ![pic](./Gallery/grid_0_cm.png) | ![pic](./Gallery/pattern_0_cm.png) |
| 48 bi-linear elements, <br/>114 dofs  | ![pic](./Gallery/grid_1_cm.png) | ![pic](./Gallery/pattern_1_cm.png) |
| 192 bi-linear elements, <br/>418 dofs | ![pic](./Gallery/grid_2_cm.png) | ![pic](./Gallery/pattern_2_cm.png) |
