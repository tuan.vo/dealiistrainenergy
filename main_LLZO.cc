//Include files
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

#include "FEM_LLZO_spatial_multicoeff_binary_inclusions_target1_inmfdo_ouvnD.h"

using namespace dealii;

// //The main program, using the FEM class
// int main()
// {
// 	try
// 	{
// 		deallog.depth_console(0);
// 		const int dimension_of_problem = 2;
// 		//Time integration
// 		//Define Newmark method (0 <= gamma <= 1) and (0 <= beta <= 1/2)
// 		double gamma = 0.5,
// 			   beta = 0.25;

// 		FEM<dimension_of_problem> LLZO_Solid(gamma, beta);

// 		LLZO_Solid.generate_grid();

// 		LLZO_Solid.distribute_dofs();
// 		LLZO_Solid.redistribute_dofs();
// 		LLZO_Solid.random_distribute_dofs();

// 		LLZO_Solid.setup_system();
// 		LLZO_Solid.assembling();

// 		//Direct solver
// 		//LLZO_Solid.solution_steady();

// 		//Iteration solver: Conjugate gradient
// 		LLZO_Solid.solution_steady_CG();

// 		//LLZO_Solid.solution_transient();

// 		//Strain energy density
// 		LLZO_Solid.senergy();
// 	}
//---------------------------------------------------------------------------	
int main(int argc, char *argv[])
{
	try
	{
		deallog.depth_console(0);
		std::cout << "You have entered " << argc << " arguments." << std::endl;
		//Convert char argv[1] to double
		double doubleargv1 = atof(argv[1]);
		double doubleargv2 = atof(argv[2]);
		std::cout << "You have entered " << doubleargv1 << " for Young E." << std::endl;
		std::cout << "You have entered " << doubleargv2 << " for Poisson nu." << std::endl;

		const int dimension_of_problem = 2;
		//Time integration
		//Define Newmark method (0 <= gamma <= 1) and (0 <= beta <= 1/2)
		double gamma = 0.5,
			   beta = 0.25;

		FEM<dimension_of_problem> LLZO_Solid(gamma, beta, doubleargv1, doubleargv2);

		LLZO_Solid.generate_grid();

		LLZO_Solid.distribute_dofs();
		LLZO_Solid.redistribute_dofs();
		LLZO_Solid.random_distribute_dofs();

		LLZO_Solid.setup_system();
		LLZO_Solid.assembling();

		//Direct solver
		//LLZO_Solid.solution_steady();

		//Iteration solver: Conjugate gradient
		LLZO_Solid.solution_steady_CG();

		//LLZO_Solid.solution_transient();

		//Strain energy density
		LLZO_Solid.senergy();
	}
	//---------------------------------------------------------------------------
	//  Other checkings
	//---------------------------------------------------------------------------
	catch (std::exception &exc)
	{
		std::cerr << std::endl
				  << std::endl
				  << "----------------------------------------------------"
				  << std::endl;
		std::cerr << "Exception on processing: " << std::endl
				  << exc.what() << std::endl
				  << "Aborting!" << std::endl
				  << "----------------------------------------------------"
				  << std::endl;

		return 1;
	}
	catch (...)
	{
		std::cerr << std::endl
				  << std::endl
				  << "----------------------------------------------------"
				  << std::endl;
		std::cerr << "Unknown exception!" << std::endl
				  << "Aborting!" << std::endl
				  << "----------------------------------------------------"
				  << std::endl;
		return 1;
	}

	return 0;
}