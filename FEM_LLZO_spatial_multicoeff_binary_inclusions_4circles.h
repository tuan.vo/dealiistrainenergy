#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>

#include <deal.II/base/timer.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/tensor_function.h>
#include <deal.II/base/tensor.h>
#include <deal.II/base/quadrature.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/dofs/dof_renumbering.h>

#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/fe_q.h>
#include <deal.II/fe/mapping_q1.h>

#include <deal.II/grid/tria.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/manifold_lib.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/grid/grid_out.h>

#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/sparse_direct.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/precondition.h>
#include <deal.II/lac/matrix_out.h>

#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/data_postprocessor.h>

using namespace dealii;

//---------------------------------------------------------------------------------------------------------------------- Lagrange basic function
//The Lagrange-type basis functions are defined directly from deal.ii
const unsigned int basis_fcn_order = 1;

//---------------------------------------------------------------------------------------------------------------------- Gauss quadrature
//The order of Gauss quadrature rule are defined directly from deal.ii
const unsigned int quadRule = 2;

//---------------------------------------------------------------------------------------------------------------------- Refinement degree
//Number of elements used for the discretized domain
const unsigned int refinement_degree = 1;
// 0:  12 elements -->  17 nodes =  34 dofs
// 1:  48 elements -->  57 nodes = 114 dofs
// 2: 192 elements --> 209 nodes = 418 dofs

//---------------------------------------------------------------------------------------------------------------------- Class definitions
template <int dim>
class FEM // This is a class oriented for FEM of LLZO solid: Below inludes functions and variables
{
public:
  FEM(double Gamma, double Beta, double YoungE, double PoissonNu);
  ~FEM();

  //Fourth-order elasticity tensor C, Second-order structure tensor M, Vectorial direction a
  double C(unsigned int i, unsigned int j, unsigned int k, unsigned int l, double x, double y);

  double multi_coeff_E(const Point<dim> &p);
  double multi_coeff_nu(const Point<dim> &p);

  double struct_MR(unsigned int i, unsigned int j);
  double struct_ME(unsigned int i, unsigned int j);

  double direct_dR(unsigned int i);
  double direct_dE(unsigned int i);

  void Gauss_Points_Weights();

  //Canonical functions for FEM step-by-step
  void generate_grid();
  void distribute_dofs();
  void redistribute_dofs();
  void random_distribute_dofs();

  void apply_Dirichlet_BCs();

  void setup_system();
  void assembling();

  void solution_steady();
  void solution_steady_CG();

  //Functions for transient problem
  void apply_ICs();
  void solution_transient();

  //Output functions
  void output_results();
  void output_trans_results(unsigned int index); //For every #index the results will be saved into a separate file.

  //Post-processing functions: Strain energy
  void senergy();

  //----------------------------------------------------------------------------------------------------------------------
  //Class objects from deal.II library
  Triangulation<dim> triangulation; //Meshing
  FESystem<dim> fe;                 //FE element
  DoFHandler<dim> dof_handler;      //Connectivity matrices

  //The deal.II Gauss quadratures: Volume and surface
  QGauss<dim> quadrature_formula;          //Volume Quadrature
  QGauss<dim - 1> edge_quadrature_formula; //Surface Quadrature

  //Data structures
  SparsityPattern sparsity_pattern;                                //Sparse matrix pattern
  SparseMatrix<double> M, Rayleigh, K, system_matrix;              //Global stiffness matrix - Sparse matrix - used in the solver
  Vector<double> D, D_trans, V_trans, A_trans, F, RHS, RHS1, RHS2; //Global vectors - Solution vector (D) and Global force vector (F)

  //---------------------------------------------------------------------------------------------------------------------- Table dofLocation
  //In 3D problem of displacement, i.e. u, there are 3 dofs per node.
  //For example: dof 0,1 and 2 would have the same location,
  //             as they refer to the same node,
  //             but each has their own role corresponding to x, y and z direction
  Table<2, double> dofLocation;                        //Table of the coordinates of dofs by global dof number
  std::map<unsigned int, double> BC_Dirichlet_values;  //Map of dirichlet boundary conditions: for steady state
  std::map<unsigned int, double> boundary_values_of_A; //Map of dirichlet boundary conditions: for transient

  //Coefficients for Newmark method used for transient problem
  double gamma, beta;

  // Young E_ and Poisson nu_ taken from argument C++
  double E_, nu_;

  //----------------------------------------------------------------------------------------------------------------------
  //solution name array
  std::vector<std::string> nodal_solution_names;
  std::vector<DataComponentInterpretation::DataComponentInterpretation> nodal_data_component_interpretation;
};

//---------------------------------------------------------------------------------------------------------------------- Class FEM constructor
// Class constructor for a vector field
template <int dim>
FEM<dim>::FEM(double Gamma, double Beta, double YoungE, double PoissonNu)
    : fe(FE_Q<dim>(basis_fcn_order), dim),
      dof_handler(triangulation),
      quadrature_formula(quadRule),
      edge_quadrature_formula(quadRule)
{
  gamma = Gamma;
  beta = Beta;
  E_ = YoungE;
  nu_ = PoissonNu;
  //Nodal solution names: displacement u is a vector field instead of a scalar field
  for (unsigned int i = 0; i < dim; ++i)
  {
    nodal_solution_names.push_back("displacement_u");
    nodal_data_component_interpretation.push_back(DataComponentInterpretation::component_is_part_of_vector);
  }
}

//---------------------------------------------------------------------------------------------------------------------- Class FEM destructor
//Class destructor
template <int dim>
FEM<dim>::~FEM() { dof_handler.clear(); }

//---------------------------------------------------------------------------------------------------------------------- Main functions starting ...
//                    MAIN FUNCTIONS ARE DEFINED FROM NOW ON
//----------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------- d_R-direction
//Preferred-direction vector d_R as Reference direction
template <int dim>
double FEM<dim>::direct_dR(unsigned int i)
{
  double phi = M_PI / 6.0;
  double drt_dR;
  if (i == 0)
  {
    drt_dR = cos(phi);
  }; // x-direction = cos(phi)
  if (i == 1)
  {
    drt_dR = sin(phi);
  }; // y-direction = sin(phi)

  return drt_dR;
}

//---------------------------------------------------------------------------------------------------------------------- d_E-direction
//Preferred-direction vector d_E caused by dielectric polarization
template <int dim>
double FEM<dim>::direct_dE(unsigned int i)
{
  double phi = M_PI / 6.0 + M_PI / 2.0;
  double drt_dE;
  if (i == 0)
  {
    drt_dE = cos(phi);
  }; // x-direction = cos(phi)
  if (i == 1)
  {
    drt_dE = sin(phi);
  }; // y-direction = sin(phi)

  return drt_dE;
}

//---------------------------------------------------------------------------------------------------------------------- MR Structure
// Structure tensor MR = d_R (dyadic) d_R
template <int dim>
double FEM<dim>::struct_MR(unsigned int i, unsigned int j)
{
  double sM;
  sM = direct_dR(i) * direct_dR(j);

  return sM;
}

//---------------------------------------------------------------------------------------------------------------------- ME Structure
// Structure tensor ME = d_E (dyadic) d_E
template <int dim>
double FEM<dim>::struct_ME(unsigned int i, unsigned int j)
{
  double sME;
  sME = direct_dE(i) * direct_dE(j);
  return sME;
}

//---------------------------------------------------------------------------------------------------------------------- C-Modulus
//Function to calculate the components of the 4th order elasticity tensor: isotropic material
// C = lambda 1(dyadic)1 + 2 mu II
// C(ijkl) = lambda delta(ij) delta(kl) + 2 mu ( 1/2 ( delta(ik)delta(jl)) + delta(il)delta(jk) )
template <int dim>
double FEM<dim>::C(unsigned int i, unsigned int j, unsigned int k, unsigned int l, double val_E, double val_nu)
{
  //Initialization
  double Cmodulus = 0.0;
  double val_ld = 0.0;
  double val_mu = 0.0;

  //Convert from (E,nu) to Lame constants (lambda,mu)
  val_ld = (val_E * val_nu) / ((1.0 + val_nu) * (1.0 - 2.0 * val_nu));
  val_mu = val_E / (2.0 * (1.0 + val_nu));

  Cmodulus =
      val_ld * (i == j) * (k == l) 
    + val_mu * ((i == k) * (j == l) + (i == l) * (j == k));

  //-------------------------------------------------------------------------- Case C 5th - Spatial dependence
  //Case 5th: fGL applied for the two Lame constants lambda and mu
  //        + fGL applied for angle across grain boundary

  //-------------------------------------------------------------------------- Case C 6th - Spatial dependence
  //Case 6th: fGL applied for the two Lame constants lambda and mu
  //        + fGL applied for angle across grain boundary
  //        + dE w/o fGL

  return Cmodulus;
}

//---------------------------------------------------------------------------------------------------------------------- Definition of 2D-domain hyper_ball_balanced
//Define the problem domain and generate the mesh
template <int dim>
void FEM<dim>::generate_grid()
{
  std::cout << "------------------------------------------------------------------ " << std::endl;
  std::cout << "Entering generate_grid... " << std::endl;

  const Point<dim> center(0.0, 0.0);
  const double radius = 0.5;
  GridGenerator::hyper_ball_balanced(triangulation, center, radius);
  triangulation.refine_global(refinement_degree);

  std::ofstream out("grid-ball.svg");
  GridOut grid_ball_output;
  grid_ball_output.write_svg(triangulation, out);
  std::cout << "Output of grid_ball has been generated and visualized. The shape can be seen at grid-ball.svg file!" << std::endl;

  std::cout << "------------------------------------------------------------------ " << std::endl;
  std::cout << " This is hyper_ball_balanced" << std::endl;
  std::cout << "   Number of active elements   : " << triangulation.n_active_cells() << std::endl;
}

//---------------------------------------------------------------------------------------------------------------------- Sparsity analysis of 2D-domain hyper_ball_balanced
//Print out the pattern of sparsity
template <int dim>
void FEM<dim>::distribute_dofs()
{
  //const FE_Q<dim> finite_element(1); //order: linear 1, quadratic 2, cubic 3... basis functions

  dof_handler.distribute_dofs(fe);

  DynamicSparsityPattern dynamic_sparsity_pattern(dof_handler.n_dofs(), dof_handler.n_dofs());

  DoFTools::make_sparsity_pattern(dof_handler, dynamic_sparsity_pattern);

  SparsityPattern sparsity_pattern;

  sparsity_pattern.copy_from(dynamic_sparsity_pattern);

  //Print to print_svg
  std::ofstream out_svg("matrix_sparsity_pattern.svg");
  sparsity_pattern.print_svg(out_svg);

  //Print to print_gnuplot
  std::ofstream out_gnuplot("matrix_sparsity_pattern.gpl");
  sparsity_pattern.print_gnuplot(out_gnuplot);

  //Print to print_gnuplot with numbering
  std::ofstream out("matrix_sparsity_pattern_numbering.gpl");
  out << "plot '-' with lines, "
      << "'-' with labels point pt 2 offset 1,1"
      << std::endl;

  GridOut().write_gnuplot(triangulation, out);
  out << "e" << std::endl;

  std::map<types::global_dof_index, Point<dim>> support_points;
  DoFTools::map_dofs_to_support_points(MappingQ1<dim>(), dof_handler, support_points);

  DoFTools::write_gnuplot_dof_support_point_info(out, support_points);
  out << "e" << std::endl;

  std::cout << "Sparsity pattern of grid_ball has been generated. The pattern can be seen at matrix_sparsity_pattern_numbering.svg file!" << std::endl;
}

//---------------------------------------------------------------------------------------------------------------------- Sparsity analysis of 2D-domain hyper_ball_balanced
//Print out the pattern of sparsity
template <int dim>
void FEM<dim>::redistribute_dofs()
{

  DoFRenumbering::Cuthill_McKee(dof_handler);

  DynamicSparsityPattern dynamic_sparsity_pattern(dof_handler.n_dofs(), dof_handler.n_dofs());

  DoFTools::make_sparsity_pattern(dof_handler, dynamic_sparsity_pattern);

  SparsityPattern sparsity_pattern;

  sparsity_pattern.copy_from(dynamic_sparsity_pattern);

  //Print to print_svg
  std::ofstream out_svg("re_matrix_sparsity_pattern.svg");
  sparsity_pattern.print_svg(out_svg);

  //Print to print_gnuplot
  std::ofstream out_gnuplot("re_matrix_sparsity_pattern.gpl");
  sparsity_pattern.print_gnuplot(out_gnuplot);

  //Print to print_gnuplot with numbering
  std::ofstream out("re_matrix_sparsity_pattern_numbering.gpl");
  out << "plot '-' with lines, "
      << "'-' with labels point pt 2 offset 1,1"
      << std::endl;

  GridOut().write_gnuplot(triangulation, out);
  out << "e" << std::endl;

  std::map<types::global_dof_index, Point<dim>> support_points;
  DoFTools::map_dofs_to_support_points(MappingQ1<dim>(), dof_handler, support_points);

  DoFTools::write_gnuplot_dof_support_point_info(out, support_points);
  out << "e" << std::endl;

  std::cout << "(Re)-Sparsity pattern of grid_ball has been generated. The pattern can be seen at re_matrix_sparsity_pattern_numbering.svg file!" << std::endl;
}

//---------------------------------------------------------------------------------------------------------------------- Sparsity analysis of 2D-domain hyper_ball_balanced
//Print out the pattern of sparsity
template <int dim>
void FEM<dim>::random_distribute_dofs()
{

  DoFRenumbering::random(dof_handler);

  DynamicSparsityPattern dynamic_sparsity_pattern(dof_handler.n_dofs(), dof_handler.n_dofs());

  DoFTools::make_sparsity_pattern(dof_handler, dynamic_sparsity_pattern);

  SparsityPattern sparsity_pattern;

  sparsity_pattern.copy_from(dynamic_sparsity_pattern);

  //Print to print_svg
  std::ofstream out_svg("random_matrix_sparsity_pattern.svg");
  sparsity_pattern.print_svg(out_svg);

  //Print to print_gnuplot
  std::ofstream out_gnuplot("random_matrix_sparsity_pattern.gpl");
  sparsity_pattern.print_gnuplot(out_gnuplot);

  //Print to print_gnuplot with numbering
  std::ofstream out("random_matrix_sparsity_pattern_numbering.gpl");
  out << "plot '-' with lines, "
      << "'-' with labels point pt 2 offset 1,1"
      << std::endl;

  GridOut().write_gnuplot(triangulation, out);
  out << "e" << std::endl;

  std::map<types::global_dof_index, Point<dim>> support_points;
  DoFTools::map_dofs_to_support_points(MappingQ1<dim>(), dof_handler, support_points);

  DoFTools::write_gnuplot_dof_support_point_info(out, support_points);
  out << "e" << std::endl;

  std::cout << "(Random)-Sparsity pattern of grid_ball has been generated. The pattern can be seen at random_matrix_sparsity_pattern_numbering.svg file!" << std::endl;
}

//---------------------------------------------------------------------------------------------------------------------- Dirichlet boundary conditions
//Specify the Dirichlet boundary conditions
template <int dim>
void FEM<dim>::apply_Dirichlet_BCs()
{
  const unsigned int totalDOFs = dof_handler.n_dofs(); //Total number of degrees of freedom

  for (unsigned int globalDof = 0; globalDof < totalDOFs; globalDof++)
  {
    /*std::cout << "Checking dofLocation[" << globalDof << "][0]: " << dofLocation[globalDof][0] << std::endl;
    std::cout << "Checking dofLocation[" << globalDof << "][1]: " << dofLocation[globalDof][1] << std::endl;*/

    // Fixed most-left point (0.5,0.0) to zeros
    if (dofLocation[globalDof][0] == -0.5 && dofLocation[globalDof][1] == 0.0)
    {
      BC_Dirichlet_values[globalDof] = 0.0;
    }
  }
}

//---------------------------------------------------------------------------------------------------------------------- multi_coeff_ld
//Specify coefficient
template <int dim>
double FEM<dim>::multi_coeff_E(const Point<dim> &p)
{ 
  //Scalar product of vector p with itself
  if (p.square() < 0.25 * 0.25 && p.square() > 0.15 * 0.15)
    //--------------------------- small circles
    // LLZO-noDoped-LL: median value
    //return 155.7e9;
    // LLZO-noDoped-HL: median value
    //return 71.2e9;
    std::cout << "--------------------------------------------------------Yes I have found these points: " << p << std::endl;
  else
    return this->E_;
}

// template <int dim>
// double FEM<dim>::distance(const Point<dim> &p1, const Point<dim> &p2, double sradius)
// { 
//   double dis = 0.0;
//   dis = (p2[1]-p1[1])*(p2[1]-p1[1]) + (p2[0]-p1[0])*(p2[0]-p1[0]);
//   return ;
// }

//---------------------------------------------------------------------------------------------------------------------- multi_coeff_mu
//Specify coefficient
template <int dim>
double FEM<dim>::multi_coeff_nu(const Point<dim> &p)
{ 
  //Scalar product of vector p with itself
  if (p.square() < 0.3 * 0.3)
    return 0.38;
  else
    return this->nu_;
}

//---------------------------------------------------------------------------------------------------------------------- Setup the system
//Setup data structures (sparse matrix, vectors)
template <int dim>
void FEM<dim>::setup_system()
{

  std::cout << "------------------------------------------------------------------ " << std::endl;
  std::cout << "Setting up the system... " << std::endl;
  Timer timer;
  timer.start();

  //Deal.II distributes dofs
  //dof_handler.distribute_dofs(fe);

  //Self-distribution
  distribute_dofs();

  //Self-distribution with redistribution
  //distribute_dofs();
  //redistribute_dofs();

  //Self-distribution with random distribution
  //distribute_dofs();
  //random_distribute_dofs();

  //Get a vector of global degree-of-freedom x-coordinates
  MappingQ1<dim, dim> mapping;

  //dof_coords[number of dofs in the whole system][dim] = 0
  std::vector<Point<dim, double>> dof_coords(dof_handler.n_dofs());

  //dofLocation[number of dofs in the whole system][dim] = value in physical domain (coordinate)
  dofLocation.reinit(dof_handler.n_dofs(), dim);

  //map dofs
  DoFTools::map_dofs_to_support_points<dim, dim>(mapping, dof_handler, dof_coords);

  std::cout << "dof_coords[10][0] " << dof_coords[10][0] << std::endl;
  std::cout << "dof_coords[10][1] " << dof_coords[10][1] << std::endl;

  for (unsigned int i = 0; i < dof_coords.size(); i++)
  {
    for (unsigned int j = 0; j < dim; j++)
    {
      dofLocation[i][j] = dof_coords[i][j];
    }
  }

  std::cout << "dofLocation[10][0] " << dofLocation[10][0] << std::endl;
  std::cout << "dofLocation[10][1] " << dofLocation[10][1] << std::endl;

  //---------------------------------------------------------------------------------------------------------------------- Call BCs
  apply_Dirichlet_BCs();

  //----------------------------------------------------------------------------------------------------------------------
  //Define the size of the global matrices and vectors
  sparsity_pattern.reinit(dof_handler.n_dofs(), dof_handler.n_dofs(), dof_handler.max_couplings_between_dofs());
  DoFTools::make_sparsity_pattern(dof_handler, sparsity_pattern);
  sparsity_pattern.compress();

  M.reinit(sparsity_pattern);
  Rayleigh.reinit(sparsity_pattern);
  K.reinit(sparsity_pattern);
  system_matrix.reinit(sparsity_pattern);

  D.reinit(dof_handler.n_dofs());
  D_trans.reinit(dof_handler.n_dofs());
  V_trans.reinit(dof_handler.n_dofs());
  A_trans.reinit(dof_handler.n_dofs());
  RHS1.reinit(dof_handler.n_dofs());
  RHS2.reinit(dof_handler.n_dofs());
  RHS.reinit(dof_handler.n_dofs());
  F.reinit(dof_handler.n_dofs());

  timer.stop();
  std::cout << "Elapsed CPU time: " << timer.cpu_time() << " seconds." << std::endl;
  std::cout << "Elapsed wall time: " << timer.wall_time() << " seconds." << std::endl;
  timer.reset();

  std::cout << "------------------------------------------------------------------ " << std::endl;
  //std::cout << "   Number of active elements   : " << triangulation.n_active_cells() << std::endl;
  std::cout << "   Number of degrees of freedom: " << dof_handler.n_dofs() << std::endl;
}

//---------------------------------------------------------------------------------------------------------------------- Assembling
//Form elmental vectors and matrices and assemble to the global vector (F) and matrix (K)
template <int dim>
void FEM<dim>::assembling()
{

  std::cout << "------------------------------------------------------------------ " << std::endl;
  std::cout << "Assembling... " << std::endl;
  dealii::Timer timer;
  timer.start();

  //---------------------------------------------------------------------------------------------------------------------- fe_values
  //deal.ii object: holds information about the basic fcn,  basic fcn gradient, quad points, Jacobian
  //For volume integration/quadrature points
  FEValues<dim> fe_values(fe,                            //Basic fcn order
                          quadrature_formula,            //Quadrature rule
                          update_values |                //Update basic fcn                          --> Felement_bodyforce
                              update_gradients |         //Update basic fcn gradients                --> Kelement
                              update_quadrature_points | //This one is used for Cijkl(y)fGL
                              update_JxW_values);        //Jacobian x Weight

  //---------------------------------------------------------------------------------------------------------------------- fe_face_values
  //deal.ii object: holds information about the basic fcn,  basic fcn gradient, quad points, Jacobian
  //For surface integration/quadrature points
  FEFaceValues<dim> fe_face_values(fe,                            //Basic fcn order
                                   edge_quadrature_formula,       //Quadrature rule
                                   update_values |                //Update basic fcn                 --> Felement_Neumann
                                       update_quadrature_points | //This one is used for Neumann BC
                                       update_JxW_values);        //Jacobian x Weight

  //----------------------------------------------------------------------------------------------------------------------
  M = 0.0;
  Rayleigh = 0.0;
  K = 0.0;
  F = 0.0;

  const unsigned int dofs_per_elem = fe.dofs_per_cell; //This tells number of dofs per element
  const unsigned int nodes_per_elem = GeometryInfo<dim>::vertices_per_cell;
  const unsigned int num_quad_pts = quadrature_formula.size();           //Total number of quad points in the element
  const unsigned int num_edge_quad_pts = edge_quadrature_formula.size(); //Total number of quad points on the edge
  const unsigned int edges_per_elem = GeometryInfo<dim>::faces_per_cell;

  FullMatrix<double> Melement(dofs_per_elem, dofs_per_elem);
  FullMatrix<double> Kelement(dofs_per_elem, dofs_per_elem);
  FullMatrix<double> Rayleighlocal(dofs_per_elem, dofs_per_elem);
  Vector<double> Felement_Neumann(dofs_per_elem);
  Vector<double> Felement_bodyforce(dofs_per_elem);

  std::vector<unsigned int> local_dof_indices(dofs_per_elem); //This relates local dof numbering to global dof numbering

  std::cout << "num_edge_quad_pts " << num_edge_quad_pts << std::endl;

  //Define mass density
  double rho = 3.8e6;

  //**********************************************************************************************************************
  //  LOOP OVER ELEMENT STARTS FROM HERE.
  //**********************************************************************************************************************

  //----------------------------------------------------------------------------------------------------------------------
  typename dealii::DoFHandler<dim>::active_cell_iterator elem = dof_handler.begin_active(), endc = dof_handler.end();

  //std::cout << "Print elem " << elem << std::endl;
  //std::cout << "Print endc " << endc << std::endl;

  for (; elem != endc; ++elem)
  {

    //Update fe_values for the current element
    fe_values.reinit(elem);

    //Retrieve the effective "connectivity matrix" for this element
    elem->get_dof_indices(local_dof_indices);

    std::cout << "--------------------------------------------------------------------Element of " << elem << "th:" << std::endl;

    //Print order of dofs from 1 element, i.e. the first element will look likd 01 23 45 67 (left-right)
    if (basis_fcn_order == 1)
    {
      for (unsigned int i = 0; i < (unsigned int)(pow(2, 2) + 0.5) * dim; i++)
      {
        std::cout << local_dof_indices[i] << " ";
      }
      std::cout << std::endl;
    }

    if (basis_fcn_order == 2)
    {
      for (unsigned int i = 0; i < (unsigned int)(pow(3, 2) + 0.5) * dim; i++)
      {
        std::cout << local_dof_indices[i] << " ";
      }
      std::cout << std::endl;
    }

    if (basis_fcn_order == 4)
    {
      for (unsigned int i = 0; i < (unsigned int)(pow(4, 2) + 0.5) * dim; i++)
      {
        std::cout << local_dof_indices[i] << " ";
      }
      std::cout << std::endl;
    }

    if (basis_fcn_order == 6)
    {
      for (unsigned int i = 0; i < (unsigned int)(pow(6, 2) + 0.5) * dim; i++)
      {
        std::cout << local_dof_indices[i] << " ";
      }
      std::cout << std::endl;
    }

    if (basis_fcn_order == 8)
    {
      for (unsigned int i = 0; i < (unsigned int)(pow(8, 2) + 0.5) * dim; i++)
      {
        std::cout << local_dof_indices[i] << " ";
      }
      std::cout << std::endl;
    }

    if (basis_fcn_order == 10)
    {
      for (unsigned int i = 0; i < (unsigned int)(pow(10, 2) + 0.5) * dim; i++)
      {
        std::cout << local_dof_indices[i] << " ";
      }
      std::cout << std::endl;
    }

    //---------------------------------------------------------------------------------------------------------------------- Elem length
    //Length of element is computed here

    double edge_x_1st = 0.0, edge_y_1st = 0.0, edge_length_1st = 0.0;
    double edge_x_2nd = 0.0, edge_y_2nd = 0.0, edge_length_2nd = 0.0;
    double edge_x_3rd = 0.0, edge_y_3rd = 0.0, edge_length_3rd = 0.0;
    double edge_x_4th = 0.0, edge_y_4th = 0.0, edge_length_4th = 0.0;

    //First edge
    edge_x_1st = abs(dofLocation[local_dof_indices[2]][0] - dofLocation[local_dof_indices[0]][0]);
    edge_y_1st = abs(dofLocation[local_dof_indices[3]][1] - dofLocation[local_dof_indices[1]][1]);
    edge_length_1st = sqrt(edge_x_1st * edge_x_1st + edge_y_1st * edge_y_1st);
    std::cout << "1st edge length: " << edge_length_1st << std::endl;

    //Second edge
    edge_x_2nd = abs(dofLocation[local_dof_indices[6]][0] - dofLocation[local_dof_indices[2]][0]);
    edge_y_2nd = abs(dofLocation[local_dof_indices[7]][1] - dofLocation[local_dof_indices[3]][1]);
    edge_length_2nd = sqrt(edge_x_2nd * edge_x_2nd + edge_y_2nd * edge_y_2nd);
    std::cout << "2nd edge length: " << edge_length_2nd << std::endl;

    //Third edge
    edge_x_3rd = abs(dofLocation[local_dof_indices[6]][0] - dofLocation[local_dof_indices[4]][0]);
    edge_y_3rd = abs(dofLocation[local_dof_indices[7]][1] - dofLocation[local_dof_indices[5]][1]);
    edge_length_3rd = sqrt(edge_x_3rd * edge_x_3rd + edge_y_3rd * edge_y_3rd);
    std::cout << "3rd edge length: " << edge_length_3rd << std::endl;

    //Fourth edge
    edge_x_4th = abs(dofLocation[local_dof_indices[4]][0] - dofLocation[local_dof_indices[0]][0]);
    edge_y_4th = abs(dofLocation[local_dof_indices[5]][1] - dofLocation[local_dof_indices[1]][1]);
    edge_length_4th = sqrt(edge_x_4th * edge_x_4th + edge_y_4th * edge_y_4th);
    std::cout << "4th edge length: " << edge_length_4th << std::endl;

    //---------------------------------------------------------------------------------------------------------------------- Melements
    //Loop over local DOFs and quadrature points to populate Melement
    // Melement[A][B] = int_{Omega_e} rho N^A N^B dv
    Melement = 0.0;
    for (unsigned int q = 0; q < num_quad_pts; q++)
    {
      for (unsigned int A = 0; A < nodes_per_elem; A++)
      {
        for (unsigned int i = 0; i < dim; i++)
        { //Loop over nodal dofs
          for (unsigned int B = 0; B < nodes_per_elem; B++)
          {
            Melement[2 * A + i][2 * B + i] += fe_values.shape_value(2 * A + i, q) * fe_values.shape_value(2 * B + i, q) * fe_values.JxW(q) * rho;
          }
        }
      }
    }

    //---------------------------------------------------------------------------------------------------------------------- Kelement
    // Kelement^{AB}_{ik} = Kelement[3A+i][3B+k] = int_{Omega_e} N^{A},j C_{ijkl} N^{B},l dv
    // Implied summation over j,l =, 0,1,2
    // Note: A,B = 0,...,Number of node per element
    //       i,j = 0,1,2
    //
    //                              Kelement matrix
    //
    //
    // Ele dof  0  1  2 | 3 4 5 |  6 7 8 | 91011 | 121314| 151617| 181920| 212223|
    // Ele node    0    |   1   |    2   |   3   |   4   |    5  |    6  |    7  |
    // Nodaldof 0  1  2 | 0 1 2 |  0 1 2 | 0 1 2 | 0 1 2 | 0  1 2| 0  1 2| 0  1 2|
    //          -----------------------------------------------------------------
    //   0      0       |       |        |       |       |       |       |       |
    //   1   0  1       |       |        |       |       |       |       |       |
    //   2      2       |       |        |       |       |       |       |       |
    //          -----------------------------------------------------------------
    //   3      0       |       |        |       |       |       |       |       |
    //   4   1  1       |       |        |       |       |       |       |       |
    //   5      2       |       |        |       |       |       |       |       |
    //          -----------------------------------------------------------------
    //   6      0       |       |        |       |       |       |       |       |
    //   7   2  1       |       |        |       |       |       |       |       |
    //   8      2       |       |        |       |       |       |       |       |
    //          -----------------------------------------------------------------
    //   9      0       |       |        |       |       |       |       |       |
    //  10   3  1       |       |        |       |       |       |       |       |
    //  11      2       |       |        |       |       |       |       |       |
    //          -----------------------------------------------------------------
    //  12      0       |       |        |       |       |       |       |       |
    //  13   4  1       |       |        |       |       |       |       |       |
    //  14      2       |       |        |       |       |       |       |       |
    //          -----------------------------------------------------------------
    //  15      0       |       |        |       |       |       |       |       |
    //  16   5  1       |       |        |       |       |       |       |       |
    //  17      2       |       |        |       |       |       |       |       |
    //          -----------------------------------------------------------------
    //  18      0       |       |        |       |       |       |       |       |
    //  19   6  1       |       |        |       |       |       |       |       |
    //  20      2       |       |        |       |       |       |       |       |
    //          -----------------------------------------------------------------
    //  21      0       |       |        |       |       |       |       |       |
    //  22   7  1       |       |        |       |       |       |       |       |
    //  23      2       |       |        |       |       |       |       |       |
    //          -----------------------------------------------------------------
    //
    //
    //----------------------------------------------------------------------------------------------------------------------
    Kelement = 0.0;

    std::ofstream outfile_Cijkl;
    outfile_Cijkl.open("Cijkl_spatial.txt"); // No Appended

    //This Kelement for case 4th
    for (unsigned int q = 0; q < num_quad_pts; ++q)
    { //Loop over Gauss quadrature points     : 3 points here
      for (unsigned int A = 0; A < nodes_per_elem; A++)
      { //Loop over number of nodes of 1 element: 4 nodes here --> 1st basic fcn
        for (unsigned int i = 0; i < dim; i++)
        { //Loop over number of dofs of 1 node    : 2 dofs here  --> i
          for (unsigned int B = 0; B < nodes_per_elem; B++)
          { //Loop over number of nodes of 1 element: 4 nodes here --> 2nd basic fcn
            for (unsigned int k = 0; k < dim; k++)
            { //Loop over number of dofs of 1 node    : 2 dofs here  --> k
              const double updated_multi_coeff_E =
                  multi_coeff_E(fe_values.quadrature_point(q));
              const double updated_multi_coeff_nu =
                  multi_coeff_nu(fe_values.quadrature_point(q));
              for (unsigned int j = 0; j < dim; j++)
              { //Loop over number of dofs of 1 node    : 2 dofs here  --> j
                for (unsigned int l = 0; l < dim; l++)
                { //Loop over number of dofs of 1 node    : 2 dofs here  --> l
                  //Points: x- and y-coordinate of the current quadrature point living in the current element
                  //double x = fe_values.quadrature_point(q)[0];
                  //double y = fe_values.quadrature_point(q)[1];
                  outfile_Cijkl
                      << std::setprecision(25)
                      << C(i, j, k, l, updated_multi_coeff_E, updated_multi_coeff_nu) << "\n";
                  Kelement[2 * A + i][2 * B + k] +=
                      fe_values.shape_grad(2 * A + i, q)[j] // w.r.t real element coordinate
                      * C(i, j, k, l, updated_multi_coeff_E, updated_multi_coeff_nu) * fe_values.shape_grad(2 * B + k, q)[l] * fe_values.JxW(q);
                }
              }
            }
          }
        }
      }
      std::cout << "fe_values.quadrature_point(q)[0]: " << fe_values.quadrature_point(q)[0] << std::endl;
      std::cout << "fe_values.quadrature_point(q)[1]: " << fe_values.quadrature_point(q)[1] << std::endl;
    }

    outfile_Cijkl.close();

    /* No touched!
    //This Kelement for cases 4th
    for (unsigned int q=0; q<num_quad_pts; ++q){          //Loop over Gauss quadrature points     : 3 points here  
      //for (unsigned int q2=0; q2<num_quad_pts; ++q2){          //Loop over Gauss quadrature points     : 3 points here
        for (unsigned int A=0; A<nodes_per_elem; A++){      //Loop over number of nodes of 1 element: 4 nodes here --> 1st basic fcn 
          for (unsigned int i=0; i<dim; i++){               //Loop over number of dofs of 1 node    : 2 dofs here  --> i
            for (unsigned int B=0; B<nodes_per_elem; B++){  //Loop over number of nodes of 1 element: 4 nodes here --> 2nd basic fcn
              for (unsigned int k=0; k<dim; k++){           //Loop over number of dofs of 1 node    : 2 dofs here  --> k
                for (unsigned int j=0; j<dim; j++){         //Loop over number of dofs of 1 node    : 2 dofs here  --> j
                  for (unsigned int l=0; l<dim; l++){       //Loop over number of dofs of 1 node    : 2 dofs here  --> l 
                    // Kelement^{AB}_{ik} = Kelement[3A+i][3B+k] = int_{Omega_e} N^{A}_{,j} * C_{ijkl} * N^{B}_{,l} dv
                    // Kelement[3*A+i][3*B+k] += fe_values.shape_grad(3*A+i,q)[j]*C(i,j,k,l)*fe_values.shape_grad(3*B+k,q)[l]*fe_values.JxW(q);
                    Kelement[2*A+i][2*B+k] += fe_values.shape_grad(2*A+i,q)[j]   // w.r.t real element coordinate
                                            *C(i,j,k,l,0.0,0.0)
                                            // *C(i,j,k,l,
                                            // l_e_x/2*q1+(dofLocation[local_dof_indices[2]][0]+dofLocation[local_dof_indices[0]][0])/2.0,
                                            // l_e_y/2*q2+(dofLocation[local_dof_indices[5]][1]+dofLocation[local_dof_indices[1]][1])/2.0)
                                            *fe_values.shape_grad(2*B+k,q)[l]
                                            *fe_values.JxW(q);
                  }
                }
              }
            }
          }
        }
      //}
    }*/

    //---------------------------------------------------------------------------------------------------------------------- std::cout Kelement
    //Print Kelement
    //std::cout << "--------------------------------------------------------------------Kelement of " << elem << "th:" << std::endl;
    for (unsigned int i = 0; i < nodes_per_elem * dim; i++)
    {
      std::cout << "Kelement - Line " << i << ": ";
      for (unsigned int j = 0; j < nodes_per_elem * dim; j++)
      {
        std::cout << Kelement[i][j] << " ";
      }
      std::cout << std::endl;
    }

    //---------------------------------------------------------------------------------------------------------------------- Rayleighlocal
    double coeff_M = 0.0001,
           coeff_K = 0.0;

    Rayleighlocal = 0.0;

    for (unsigned int q = 0; q < num_quad_pts; q++)
    {
      for (unsigned int A = 0; A < nodes_per_elem; A++)
      {
        for (unsigned int i = 0; i < dim; i++)
        {
          for (unsigned int B = 0; B < nodes_per_elem; B++)
          {
            Rayleighlocal[2 * A + i][2 * B + i] = coeff_M * Melement[2 * A + i][2 * B + i] +
                                                  coeff_K * Kelement[2 * A + i][2 * B + i];
          }
        }
      }
    }

    //---------------------------------------------------------------------------------------------------------------------- Felement
    //Total Felement = int (F_body) dV + int (F_traction) dS
    Felement_Neumann = 0.0;
    Felement_bodyforce = 0.0;

    //Body force - integration over volume
    //Vector<double> body_force(dim);
    double body_force = 2.0e-12;

    for (unsigned int q = 0; q < num_quad_pts; q++)
    {
      for (unsigned int A = 0; A < nodes_per_elem; A++)
      {
        for (unsigned int i = 0; i < dim; i++)
        {
          Felement_bodyforce[2 * A + i] += fe_values.shape_value(2 * A + i, q) * body_force * fe_values.JxW(q);
        }
      }
    }

    //---------------------------------------------------------------------------------------------------------------------- std::cout Felement_bodyforce
    for (unsigned int i = 0; i < nodes_per_elem * dim; i++)
    {
      std::cout << "Felement_bodyforce[" << i << "]: " << Felement_bodyforce[i] << std::endl;
    }

    //Traction: Neumann BC --> Integration over surface
    Vector<double> traction_upper(dim);
    Vector<double> traction_lower(dim);
    traction_upper = 0.0;
    traction_lower = 0.0;

    //Loop over 4 edges of the quadrilateral element
    for (unsigned int edgeid = 0; edgeid < edges_per_elem; edgeid++)
    {
      //Update fe_face_values from current element and face
      fe_face_values.reinit(elem, edgeid);

      //Upper-circle part
      if (elem->face(edgeid)->center()[0] <= 0.3 &&
          elem->face(edgeid)->center()[0] >= -0.3 &&
          elem->face(edgeid)->center()[1] >= 0.4)
      {
        for (unsigned int q = 0; q < num_edge_quad_pts; ++q)
        { //If quadRule = 2, then num_edge_quad_pts = 2
          //Points: x-coordinate and y-coordinate of the current quadrature point living on current edge
          double x_quadrature = fe_face_values.quadrature_point(q)[0];
          double y_quadrature = fe_face_values.quadrature_point(q)[1];
          std::cout << "double x = fe_face_values.quadrature_point(q)[0]: " << x_quadrature << std::endl;
          std::cout << "double y = fe_face_values.quadrature_point(q)[1]: " << y_quadrature << std::endl;
          traction_upper[1] = +1.0e+3 * abs(x_quadrature);
          for (unsigned int A = 0; A < nodes_per_elem; A++)
          {
            for (unsigned int i = 0; i < dim; i++)
            {
              Felement_Neumann[2 * A + i] += fe_face_values.shape_value(2 * A + i, q) * fe_face_values.JxW(q) * traction_upper[i];
              //std::cout << "Felement_Neumann_upper: " << Felement_Neumann[2*A+1] << std::endl;
            }
          }
        }
      }

      //Lower-circle part
      if (elem->face(edgeid)->center()[0] <= 0.3 &&
          elem->face(edgeid)->center()[0] >= -0.3 &&
          elem->face(edgeid)->center()[1] <= -0.4)
      {
        for (unsigned int q = 0; q < num_edge_quad_pts; ++q)
        { //If quadRule = 2, then num_edge_quad_pts = 2
          double x_quadrature = fe_face_values.quadrature_point(q)[0];
          double y_quadrature = fe_face_values.quadrature_point(q)[1];
          std::cout << "double x = fe_face_values.quadrature_point(q)[0]: " << x_quadrature << std::endl;
          std::cout << "double y = fe_face_values.quadrature_point(q)[1]: " << y_quadrature << std::endl;
          traction_lower[1] = -1.0e+3 * abs(x_quadrature);
          for (unsigned int A = 0; A < nodes_per_elem; A++)
          {
            for (unsigned int i = 0; i < dim; i++)
            {
              Felement_Neumann[2 * A + i] += fe_face_values.shape_value(2 * A + i, q) * fe_face_values.JxW(q) * traction_lower[i];
              //std::cout << "Felement_Neumann_lower: " << Felement_Neumann[2*A+1] << std::endl;
            }
          }
        }
      }
    }

    //---------------------------------------------------------------------------------------------------------------------- std::cout Felement_Neumann
    for (unsigned int i = 0; i < nodes_per_elem * dim; i++)
    {
      std::cout << "Felement_Neumann[" << i << "]: " << Felement_Neumann[i] << std::endl;
    }

    //---------------------------------------------------------------------------------------------------------------------- Assembling M, K, F
    //Assemble Melement, Kelement and Felement_bodyforce + Felement_Neumann into Mglobal, Kglobal, Rayleighglobal and Fglobal
    for (unsigned int i = 0; i < dofs_per_elem; i++)
    {
      F[local_dof_indices[i]] += Felement_Neumann[i];
      //F[local_dof_indices[i]]  += Felement_bodyforce[i];
      //F[local_dof_indices[i]]  += Felement_Neumann[i]+Felement_bodyforce[i];
      for (unsigned int j = 0; j < dofs_per_elem; j++)
      {
        M.add(local_dof_indices[i], local_dof_indices[j], Melement[i][j]);
        K.add(local_dof_indices[i], local_dof_indices[j], Kelement[i][j]);
        Rayleigh.add(local_dof_indices[i], local_dof_indices[j], Rayleighlocal[i][j]);
      }
    }
  }

  //**********************************************************************************************************************
  //  LOOP OVER ELEMENT HAS BEEN ENDED HERE.
  //**********************************************************************************************************************

  //---------------------------------------------------------------------------------------------------------------------- Checking K
  std::cout << "------------------------------------------------------------------ " << std::endl;
  std::cout << "K size mxn : " << K.m() << "x" << K.n() << std::endl;
  //std::cout << "Size of row 1 of K : " << K.get_row_length(0) << std::endl;
  std::cout << "n_actually_nonzero_elements() : " << K.n_actually_nonzero_elements(0.000000000000000001) << std::endl;
  std::cout << "n_nonzero_elements() : " << K.n_nonzero_elements() << std::endl;
  //std::cout << "get_sparsity_pattern() : " << K.get_sparsity_pattern() << std::endl;
  std::cout << "memory_consumption() : " << K.memory_consumption() << " bytes" << std::endl;

  //Print out K(i,j)
  //Matrix K cannot be printed in its fullness because of definition of sparsity pattern, i.e. zero entries are no stored!
  /*if (basis_fcn_order == 1){
    for (unsigned int i=0; i<8;i++){
      std::cout << "---------------" << i << std::endl;
      for (unsigned int j=0; j<8;j++){
        std::cout << K(i,j) << std::endl;    
      }
    }
    std::cout << std::endl;
  }*/

  //---------------------------------------------------------------------------------------------------------------------- Checking F

  //Case of 12 elements
  if (refinement_degree == 0)
  {
    for (unsigned int i = 0; i < F.size(); i++)
    {
      std::cout << "F[" << i << "]" << F[i] << std::endl;
    }
    std::cout << std::endl;
  }

  //Case of 48 elements
  if (refinement_degree == 1)
  {
    for (unsigned int i = 0; i < F.size(); i++)
    {
      std::cout << "F[" << i << "]" << F[i] << std::endl;
    }
    std::cout << std::endl;
  }

  //Case of 192 elements
  if (refinement_degree == 2)
  {
    for (unsigned int i = 0; i < F.size(); i++)
    {
      std::cout << "F[" << i << "]" << F[i] << std::endl;
    }
    std::cout << std::endl;
  }

  //----------------------------------------------------------------------------------------------------------------------
  //Dirichlet BCs are applied without the changed in size of Kglobal and Fglobal
  //MatrixTools::apply_boundary_values (BC_Dirichlet_values, K, D, F, false);

  timer.stop();
  std::cout << "Elapsed CPU time: " << timer.cpu_time() << " seconds." << std::endl;
  std::cout << "Elapsed wall time: " << timer.wall_time() << " seconds." << std::endl;
  timer.reset();
}

//---------------------------------------------------------------------------------------------------------------------- Initial conditions
//Apply initial conditions for the transient problem
template <int dim>
void FEM<dim>::apply_ICs()
{
  //Loop over global dofs by using dofLocation to point to the dof to define displacement u_{i} in D_trans.
  //Total number of dofs
  const unsigned int totalDofs = dof_handler.n_dofs();

  for (unsigned int i = 0; i < totalDofs; i++)
  {
    if (dofLocation[i][1] == 0.6)
    {
      D_trans[i] = 0.0;
      V_trans[i] = 0.0;
    }
    /*else if (dofLocation[i][1] >= 0.6){
      D_trans[i] = 0.05;
      V_trans[i] = 0.0;    
      }*/
  }

  //---------------------------------------------------------------------------------------------------------------------- Taking matrix M
  //Recall MA + RV + KD = F  ---> A = M^{-1}*(F-KD-RV) ---> A_0 = M_0^{-1}*(F_0 - K_0*D_0 - R_0*V_0)
  //Taking the big matrix M computed inside assemble_system(): system_matrix = M
  system_matrix.copy_from(M);

  //---------------------------------------------------------------------------------------------------------------------- RHS
  //Define RHS = F_0 - R*V_0 - K*D_0 ---> Set the RHS equal to (R times V_trans) (K times D_trans)
  K.vmult(RHS1, D_trans);        //RHS1 = K*D_trans
  Rayleigh.vmult(RHS2, V_trans); //RHS2 = Rayleigh*V_trans

  RHS1 *= -1.0; //RHS1 = -1.0*RHS1 = -K*D_trans
  RHS2 *= -1.0; //RHS2 = -1.0*RHS2 = -C*V_trans

  RHS.add(1.0, RHS1); //RHS = RHS + 1.0*RHS1 =   - K*D_trans
  RHS.add(1.0, RHS2); //RHS = RHS + 1.0*RHS2 =   - K*D_trans -C*V_trans
  RHS.add(1.0, F);    //RHS = RHS + 1.0*F    = F - K*D_trans -C*V_trans

  //----------------------------------------------------------------------------------------------------------------------
  MatrixTools::apply_boundary_values(boundary_values_of_A, system_matrix, A_trans, RHS, false);

  //----------------------------------------------------------------------------------------------------------------------
  //Solve for solution in system_matrix*V_trans = RHS
  SparseDirectUMFPACK A;       //Create matrix A based on SparseDirectUMFPACK
  A.initialize(system_matrix); //Put system_matrix, i.e. M, into A where A is defined based on SparseDirectUMFPACK
  A.vmult(A_trans, RHS);       //Compute A*RHS and put into V_trans. But in fact, it computes as V_trans=system_matrix^{-1}*RHS
  //Note: (Sparse matrix from UMFPACK) (dot) (vmult) does the matrix inversion
  //      (Sparse matrix from xxxxxxx) (dot) (vmult) does the simple matrix-vector multiplication (no matrix inversion)

  //Output initial state
  output_trans_results(0);
}

//---------------------------------------------------------------------------------------------------------------------- Solve D_trans
template <int dim>
void FEM<dim>::solution_transient()
{

  //Call the function to initialize D_trans, V_trans, A_trans as D_0, V_0 and A_0
  apply_ICs();

  //Define time increment delta_t
  const double delta_t = 0.001;

  //Total number of dofs = (3 times number of nodes)
  const unsigned int totalDofs = dof_handler.n_dofs();

  //Setup D_tilde which is used to update D_{n} to D_{n+1}
  Vector<double> D_tilde(totalDofs);

  //Setup V_tilde which is used to update V_{n} to V_{n+1}
  Vector<double> V_tilde(totalDofs);

  //--------------------------------------------------------------------------------------------------------------------------------------- Loop over time
  for (unsigned int t_step = 1; t_step < 20; t_step++)
  {

    //--------------------------------------------------------------------------------------------------------------------------------------- Predictor: D_tilde/V_tilde
    //Predictors -- Herein: D_trans = D_n and V_trans = V_n

    //Find D_tilde = D_n + delta_t*V_n + (1/2)*delta_t*delta_t*(1-2*beta)*A_n
    D_tilde = D_trans;
    D_tilde.add(delta_t, V_trans);
    D_tilde.add(delta_t * delta_t * 0.5 * (1.0 - 2.0 * beta), A_trans);

    //Find V_tilde = V_n + delta_t*(1-gamma)*A_n
    V_tilde = V_trans;
    V_tilde.add(delta_t * (1.0 - gamma), A_trans);

    //--------------------------------------------------------------------------------------------------------------------------------------- system_matrix M
    system_matrix.copy_from(M);
    system_matrix.add(delta_t * gamma, Rayleigh);
    system_matrix.add(delta_t * delta_t * beta, K);

    //--------------------------------------------------------------------------------------------------------------------------------------- RHS
    K.vmult(RHS1, D_tilde);        //RHS1 = K*D_trans
    Rayleigh.vmult(RHS2, V_tilde); //RHS2 = K*D_trans

    RHS1 *= -1.0; //RHS1 = -1.0*RHS1 = -K*D_trans
    RHS2 *= -1.0; //RHS2 = -1.0*RHS2 = -C*V_trans

    RHS.add(1.0, RHS1); //RHS = RHS + 1.0*RHS1 =   - K*D_trans
    RHS.add(1.0, RHS2); //RHS = RHS + 1.0*RHS2 =   - K*D_trans -C*V_trans
    RHS.add(1.0, F);    //RHS = RHS + 1.0*F    = F - K*D_trans -C*V_trans

    //---------------------------------------------------------------------------------------------------------------------------------------
    //Apply boundary conditions on A_trans before solving the matrix/vector system
    MatrixTools::apply_boundary_values(boundary_values_of_A, system_matrix, A_trans, RHS, false);

    //--------------------------------------------------------------------------------------------------------------------------------------- A_trans solve
    //Solve for A_trans (A_{n+1}) in system_matrix*solution = RHS --> A_trans=system_matrix^{-1}*RHS
    SparseDirectUMFPACK A;
    A.initialize(system_matrix);
    A.vmult(A_trans, RHS);

    //--------------------------------------------------------------------------------------------------------------------------------------- Corrector: D_trans/V_trans
    //Corrector -- Herein: Update D_trans/V_trans to D_{n+1}/V_{n+1} using D_tilde and A_trans (V_{n+1})

    //Update D_trans to D_trans_{n+1}
    D_trans = D_tilde;
    D_trans.add(delta_t * delta_t * beta, A_trans);

    //Update V_trans to V_trans_{n+1}
    V_trans = V_tilde;
    V_trans.add(delta_t * gamma, A_trans);

    //--------------------------------------------------------------------------------------------------------------------------------------- Time step
    //Output the results every 1 steps
    if (t_step % 1 == 0)
    {
      output_trans_results(t_step);
      //double current_l2norm = l2norm();
      //l2norm_results.push_back(current_l2norm);
    }
  }
}

//**********************************************************************************************************************
//  SOLUTION IS COMPUTED FROM HERE
//**********************************************************************************************************************

//---------------------------------------------------------------------------------------------------------------------- Solution steady state
template <int dim>
void FEM<dim>::solution_steady_CG()
{

  std::cout << "------------------------------------------------------------------ " << std::endl;
  std::cout << "Computing solution_steady_CG: displacement... " << std::endl;
  dealii::Timer timer;
  timer.start();

  MatrixTools::apply_boundary_values(BC_Dirichlet_values, K, D, F, false);

  SolverControl solver_control(500000, 1e-12);
  SolverCG<Vector<double>> solver(solver_control);

  PreconditionSSOR<SparseMatrix<double>> preconditioner;
  preconditioner.initialize(K, 1.2);

  solver.solve(K, D, F, PreconditionIdentity());

  //solver.solve(K,D,F,preconditioner);

  //Print out displacement D[i] solution
  for (unsigned int d = 0; d < D.size(); d++)
  {
    std::cout << "D[" << d << "] value: " << D[d] << std::endl;
  }

  timer.stop();
  std::cout << "Elapsed CPU time: " << timer.cpu_time() << " seconds." << std::endl;
  std::cout << "Elapsed wall time: " << timer.wall_time() << " seconds." << std::endl;
  timer.reset();

  //Steady results are called here
  output_results();
}

//---------------------------------------------------------------------------------------------------------------------- Solution steady state
template <int dim>
void FEM<dim>::solution_steady()
{

  std::cout << "------------------------------------------------------------------ " << std::endl;
  std::cout << "Computing solution_steady: displacement... " << std::endl;
  Timer timer;
  timer.start();

  MatrixTools::apply_boundary_values(BC_Dirichlet_values, K, D, F, false);

  //Solve for D=K^{-1}*F
  SparseDirectUMFPACK A;
  A.initialize(K);
  A.vmult(D, F);

  /*
  std::cout << "D size : " << D.size() << std::endl;
  std::cout << "D.l1_norm() : " << D.l1_norm() << std::endl;
  std::cout << "D.l2_norm() : " << D.l2_norm() << std::endl;
  std::cout << "D.lp_norm(2) : " << D.lp_norm(2) << std::endl;
  std::cout << "(D.l2_norm())² : " << D.l2_norm()*D.l2_norm() << std::endl;
  std::cout << "D.norm_sqr() : " << D.norm_sqr() << std::endl;
  std::cout << "D.linfty_norm() : " << D.linfty_norm() << std::endl;
  std::cout << "D.memory_consumption() : " << D.memory_consumption() << " bytes" << std::endl;*/

  //std::cout << "D value: " << D << std::endl;
  for (unsigned int d = 0; d < D.size(); d++)
  {
    std::cout << "D[" << d << "] value: " << D[d] << std::endl;
  }

  timer.stop();
  std::cout << "Elapsed CPU time: " << timer.cpu_time() << " seconds." << std::endl;
  std::cout << "Elapsed wall time: " << timer.wall_time() << " seconds." << std::endl;
  timer.reset();

  //Steady results are called here
  output_results();
}

//---------------------------------------------------------------------------------------------------------------------- Small strain epsilon Cijkl Cases 12345
// For Cijkl cases 1 2 3 4 5
template <int dim>
class StrainPostprocessor : public DataPostprocessorTensor<dim>
{
public:
  StrainPostprocessor() : DataPostprocessorTensor<dim>("epsilon", update_gradients)
  {
  }

  virtual void evaluate_vector_field(const DataPostprocessorInputs::Vector<dim> &D, std::vector<Vector<double>> &computed_strain) const
  {
    AssertDimension(D.solution_gradients.size(), computed_strain.size());

    for (unsigned int p = 0; p < D.solution_gradients.size(); ++p)
    {

      AssertDimension(computed_strain[p].size(), (Tensor<2, dim>::n_independent_components));

      for (unsigned int k = 0; k < dim; ++k)
        for (unsigned int l = 0; l < dim; ++l)
          computed_strain[p][Tensor<2, dim>::component_to_unrolled_index(TableIndices<2>(k, l))] = (D.solution_gradients[p][k][l] +
                                                                                                    D.solution_gradients[p][l][k]) /
                                                                                                   2.0;

      //Linear element will return only values for 4 nodes, i.e. p=0..3
      std::cout << "Strain[" << p << "] " << computed_strain[p] << std::endl;
    }
    //std::cout << "Size D.solution_gradients: " << D.solution_gradients.size() << std::endl;
  }
};
/*
//---------------------------------------------------------------------------------------------------------------------- Cauchy Stress sigma Cijkl Cases 4
//For Cijkl cases 4
template <int dim>
class StressPostprocessor : public DataPostprocessorTensor<dim>
{
public:
  
  StressPostprocessor (): DataPostprocessorTensor<dim> ("sigmaCauchy", update_gradients){}

  virtual void evaluate_vector_field(
    const DataPostprocessorInputs::Vector<dim> &D,
    std::vector<Vector<double> >               &computed_stress) const
  { 
    FEM<dim> FEMobj(0.25, 0.5);
    FEMobj.generate_grid();
    FEMobj.setup_system();

    FESystem<dim> fe;            
    QGauss<dim>   quadrature_formula; 

    FEMObj.quadrature_formula(2);

    //---------------------------------------------------------------------------------------------------------------------- fe_values
    //deal.ii object: holds information about the basic fcn,  basic fcn gradient, quad points, Jacobian
    //For volume integration/quadrature points
    FEValues<dim> fe_values(fe,                        //Basic fcn order
			                    quadrature_formula,        //Quadrature rule
			                    update_values |            //Update basic fcn                          --> Felement_bodyforce
			                    update_gradients |         //Update basic fcn gradients                --> Kelement
                          update_quadrature_points | //This one is used for Cijkl(y)fGL
			                    update_JxW_values);        //Jacobian x Weight

    AssertDimension (D.solution_gradients.size(), computed_stress.size());
    
    const unsigned int num_quad_pts  = FEMobj.quadrature_formula.size();
    const unsigned int dofs_per_elem = FEMobj.fe.dofs_per_cell;

    for (unsigned int p=0; p<D.solution_gradients.size();++p){
      AssertDimension(computed_stress[p].size(), (Tensor<2,dim>::n_independent_components));
        for (unsigned int i=0; i<dim; ++i)
          for (unsigned int j=0; j<dim; ++j)
            for (unsigned int k=0; k<dim; ++k)
              for (unsigned int l=0; l<dim; ++l)
                for (unsigned int q=0; q<num_quad_pts; ++q)
                {  
                  //Points: x- and y-coordinate of the current quadrature point living in the current element
                  double x = FEMObj.fe_values.quadrature_point(q)[0]; 
                  double y = FEMObj.fe_values.quadrature_point(q)[1]; 
                //std::cout << i << j << k << l << " " << FEMobj.C(i,j,k,l)
                //          << std::endl;
                  computed_stress[p][Tensor<2,dim>::component_to_unrolled_index(TableIndices<2>(i,j))]
                    += //FEMobj.C(i,j,k,l,1.0*q*l_e_y)*(D.solution_gradients[p][k][l]+D.solution_gradients[p][l][k])/2.0;
                        FEMobj.C(i,j,k,l,0.0,0.0)
                                //l_e_y/2*l_e_x/2*q+(FEMobj.dofLocation[local_dof_indices[2]][0]+FEMobj.dofLocation[local_dof_indices[0]][0])/2.0,
                                //l_e_y/2*q+(FEMobj.dofLocation[local_dof_indices[5]][1]+FEMobj.dofLocation[local_dof_indices[1]][1])/2.0)
                        *(D.solution_gradients[p][k][l]+
                          D.solution_gradients[p][l][k])/2.0;
                
                //std::cout << "Check this RHS: " << FEMobj.C(i,j,k,l)*(D.solution_gradients[p][k][l]+D.solution_gradients[p][k][l])/2.0
                //          << std::endl;
                //std::cout << "Check this one: " << (D.solution_gradients[p][k][l]+D.solution_gradients[p][k][l])/2.0
                //          << std::endl;
                }    
    }

    std::cout << "Exit for-loop---------------------" << std::endl;
    std::cout << "Stress(0): " << computed_stress[0] << std::endl;
    std::cout << "Stress(1): " << computed_stress[1] << std::endl;
    std::cout << "Stress(2): " << computed_stress[2] << std::endl;
    std::cout << "Stress(3): " << computed_stress[3] << std::endl;
    std::cout << "---------------------" << std::endl;    
  }
  //}
};*/

//---------------------------------------------------------------------------------------------------------------------- Cauchy Stress sigma Cijkl Cases 1 2 3
// For Cijkl cases 1 2 and 3
template <int dim>
class StressPostprocessor : public DataPostprocessorTensor<dim>
{
public:
  // Young E_ and Poisson nu_ taken from argument C++
  double E_StressClass = 0.0;
  double nu_StressClass = 0.0;

  StressPostprocessor(double YoungE, double PoissonNu) : DataPostprocessorTensor<dim>("sigmaCauchy", update_gradients)
  {
    E_StressClass = YoungE;
    nu_StressClass = PoissonNu;
  }

  virtual void evaluate_vector_field(const DataPostprocessorInputs::Vector<dim> &D, std::vector<Vector<double>> &computed_stress) const
  {
    FEM<dim> FEMobj(0.0, 0.0, E_StressClass, nu_StressClass);

    AssertDimension(D.solution_gradients.size(), computed_stress.size());

    //Create a text string, which is used to output the text file
    std::string myOut_Cijkl;

    //Read from the text file
    std::ifstream MyReadFile("Cijkl_spatial.txt");

    std::vector<double> C_vector;

    //Use a while loop together with the getline() function to read the file line by line
    //To extract solution values of strain and stress from out.vtk file
    while (getline(MyReadFile, myOut_Cijkl))
    {
      std::vector<double> tokens;
      // stringstream class check1
      std::stringstream check1(myOut_Cijkl);
      double x = 0.0;
      check1 >> x;
      C_vector.push_back(x);
    }

    MyReadFile.close();

    unsigned int index = 0;

    for (unsigned int p = 0; p < D.solution_gradients.size(); ++p)
    {

      AssertDimension(computed_stress[p].size(), (Tensor<2, dim>::n_independent_components));

      for (unsigned int i = 0; i < dim; ++i)
        for (unsigned int j = 0; j < dim; ++j)
          for (unsigned int k = 0; k < dim; ++k)
            for (unsigned int l = 0; l < dim; ++l)
            {
              computed_stress[p][Tensor<2, dim>::component_to_unrolled_index(TableIndices<2>(i, j))] += C_vector[index++] * (D.solution_gradients[p][k][l] + D.solution_gradients[p][l][k]) / 2.0;
            }

      // Linear element will return only values for 4 nodes, i.e. p=0..3
      std::cout << "Stress[" << p << "] " << computed_stress[p] << std::endl;
    }
    std::cout << "------------------------------------------------------------------" << std::endl;
  }
};

//**********************************************************************************************************************
//  OUTPUT - POST-PROCESSING IS FROM HERE
//**********************************************************************************************************************

//---------------------------------------------------------------------------------------------------------------------- Out steady state
//Output results
template <int dim>
void FEM<dim>::output_results()
{

  std::cout << "------------------------------------------------------------------ " << std::endl;
  std::cout << "Generating output... " << std::endl;
  Timer timer;
  timer.start();

  StrainPostprocessor<dim> epsilon;
  StressPostprocessor<dim> sigmaCauchy(E_, nu_);

  //Write results to VTK file
  std::ofstream output1("out.vtk");

  DataOut<dim> data_out;
  data_out.attach_dof_handler(dof_handler);

  std::vector<DataComponentInterpretation::DataComponentInterpretation> data_component_interpretation(dim, DataComponentInterpretation::component_is_part_of_vector);

  //Add nodal DOF data
  data_out.add_data_vector(D, nodal_solution_names, DataOut<dim>::type_dof_data, data_component_interpretation);
  data_out.add_data_vector(D, epsilon);
  data_out.add_data_vector(D, sigmaCauchy);
  data_out.build_patches();
  std::cout << "write_vtk... " << std::endl;
  data_out.write_vtk(output1);

  output1.close();

  timer.stop();
  std::cout << "Elapsed CPU time: " << timer.cpu_time() << " seconds." << std::endl;
  std::cout << "Elapsed wall time: " << timer.wall_time() << " seconds." << std::endl;
  timer.reset();
}

//---------------------------------------------------------------------------------------------------------------------- Out transient
//Output transient results for a given time step
template <int dim>
void FEM<dim>::output_trans_results(unsigned int index)
{
  //This adds an index to your filename so that you can distinguish between time steps

  //Write results to VTK file
  char filename[100];
  snprintf(filename, 100, "out_trans_%d.vtk", index);
  std::ofstream output1(filename);
  dealii::DataOut<dim> data_out;
  data_out.attach_dof_handler(dof_handler);

  //Add nodal DOF data
  data_out.add_data_vector(D_trans, nodal_solution_names, DataOut<dim>::type_dof_data, nodal_data_component_interpretation);
  data_out.build_patches();
  data_out.write_vtk(output1);
  output1.close();
}

//---------------------------------------------------------------------------------------------------------------------- Senergy
//Post-processing Strain energy
template <int dim>
void FEM<dim>::senergy()
{
  std::cout << "------------------------------------------------------------------ " << std::endl;
  std::cout << "Post-processing Strain energy... " << std::endl;
  Timer timer;
  timer.start();

  //Create a text string, which is used to output the text file
  std::string myOutVTK;

  //Read from the text file
  std::ifstream MyReadFile("out.vtk");

  std::vector<std::vector<double>> epsilon_table;
  std::vector<std::vector<double>> sigmaCauchy_table;

  //Use a while loop together with the getline() function to read the file line by line
  // To extract solution values of strain and stress from out.vtk file
  while (getline(MyReadFile, myOutVTK))
  {
    // epsilon_table
    if (myOutVTK == "SCALARS epsilon_xx double 1")
    {
      for (unsigned int i = 0; i < 4; i++)
      {
        if (i == 0)
        {
          getline(MyReadFile, myOutVTK);
          getline(MyReadFile, myOutVTK);
        }
        else
        {
          getline(MyReadFile, myOutVTK);
          getline(MyReadFile, myOutVTK);
          getline(MyReadFile, myOutVTK);
        }
        // Vector of string to save tokens
        std::vector<double> tokens;
        // stringstream class check1
        std::stringstream check1(myOutVTK);
        std::string intermediate;
        // Tokenizing w.r.t. space ' '
        while (getline(check1, intermediate, ' '))
        {
          std::stringstream geek(intermediate);
          double num = 0;
          geek >> num;
          tokens.push_back(num);
        }
        epsilon_table.push_back(tokens);
      }

      // Printing the token vector
      /*for(int i = 0; i < tokens.size(); i++)
        std::cout << tokens[i] << '\n';*/
    }

    // SigmaCauchy_table
    if (myOutVTK == "SCALARS sigmaCauchy_xx double 1")
    {
      for (unsigned int i = 0; i < 4; i++)
      {
        if (i == 0)
        {
          getline(MyReadFile, myOutVTK);
          getline(MyReadFile, myOutVTK);
        }
        else
        {
          getline(MyReadFile, myOutVTK);
          getline(MyReadFile, myOutVTK);
          getline(MyReadFile, myOutVTK);
        }
        // Vector of string to save tokens
        std::vector<double> tokens;
        // stringstream class check1
        std::stringstream check1(myOutVTK);
        std::string intermediate;
        // Tokenizing w.r.t. space ' '
        while (getline(check1, intermediate, ' '))
        {
          std::stringstream geek(intermediate);
          double num = 0;
          geek >> num;
          tokens.push_back(num);
        }
        sigmaCauchy_table.push_back(tokens);
      }

      // Printing the token vector
      /*for(int i = 0; i < tokens.size(); i++)
        std::cout << tokens[i] << '\n';*/
    }

    // Output the text from the file
    //std::cout << myOutVTK << std::endl;
  }
  //Close the file
  MyReadFile.close();

  double Senergy;
  for (unsigned int i = 0; i < 4; i++)
  {
    for (unsigned int j = 0; j < epsilon_table[0].size(); j++)
    {
      Senergy += 1.0 / 2.0 * epsilon_table[i][j] * sigmaCauchy_table[i][j];
      //std::cout << epsilon_table[i][j] * sigmaCauchy_table[i][j] << std::endl;
    }
  }

//The following are UBUNTU/LINUX, and MacOS ONLY terminal color codes.
#define RESET "\033[0m"
#define BLACK "\033[30m"              /* Black */
#define RED "\033[31m"                /* Red */
#define GREEN "\033[32m"              /* Green */
#define YELLOW "\033[33m"             /* Yellow */
#define BLUE "\033[34m"               /* Blue */
#define MAGENTA "\033[35m"            /* Magenta */
#define CYAN "\033[36m"               /* Cyan */
#define WHITE "\033[37m"              /* White */
#define BOLDBLACK "\033[1m\033[30m"   /* Bold Black */
#define BOLDRED "\033[1m\033[31m"     /* Bold Red */
#define BOLDGREEN "\033[1m\033[32m"   /* Bold Green */
#define BOLDYELLOW "\033[1m\033[33m"  /* Bold Yellow */
#define BOLDBLUE "\033[1m\033[34m"    /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m" /* Bold Magenta */
#define BOLDCYAN "\033[1m\033[36m"    /* Bold Cyan */
#define BOLDWHITE "\033[1m\033[37m"   /* Bold White */

  //Print out strain energy for 12 elements
  if (refinement_degree == 0)
  {
    std::cout << std::setprecision(25) << YELLOW << "Sum of strain energy density from 12 elements: " << RESET << BOLDYELLOW << Senergy << RESET << std::endl;

    std::ofstream outfile;
    outfile.open("Senergy.txt", std::ios_base::app); // append instead of overwrite
    outfile << Senergy << " ";
  }

  //Print out strain energy for 48 elements
  if (refinement_degree == 1)
  {
    std::cout << std::setprecision(25) << YELLOW << "Sum of strain energy density from 48 elements: " << RESET << BOLDYELLOW << Senergy << RESET << std::endl;

    std::ofstream outfile;
    outfile.open("Senergy.txt", std::ios_base::app); // append instead of overwrite
    outfile << Senergy << " ";
  }

  //Print out strain energy for 192 elements
  if (refinement_degree == 2)
  {
    std::cout << std::setprecision(25) << YELLOW << "Sum of strain energy density from 192 elements: " << RESET << BOLDYELLOW << Senergy << RESET << std::endl;

    std::ofstream outfile;
    outfile.open("Senergy.txt", std::ios_base::app); // append instead of overwrite
    outfile << Senergy << " ";
  }

  /*
  for (unsigned int i=0; i<4;i++){
    for (unsigned int j=0; j< epsilon_table[0].size();j++){
      std::cout << epsilon_table[i][j] << " ";
    }
    std::cout << std::endl;
  }*/

  timer.stop();
  std::cout << "Elapsed CPU time: " << timer.cpu_time() << " seconds." << std::endl;
  std::cout << "Elapsed wall time: " << timer.wall_time() << " seconds." << std::endl;
  timer.reset();
}

//---------------------------------------------------------------------------------------------------------------------- Gauss PW
template <int dim>
void FEM<dim>::Gauss_Points_Weights()
{
  //------------------------------------------------------------------------------------------------------------------ Gauss quadrature
  //Gauss quadrature

  /*
  //----------------------------------------------------------------------
  quadRule = 2; 
  quad_points.resize(quadRule); 
  quad_weight.resize(quadRule);

  quad_points[0] = -sqrt(1.0/3.0); 
  quad_points[1] =  sqrt(1.0/3.0); 

  quad_weight[0] = 1.0; 
  quad_weight[1] = 1.0;*/

  /*
  //----------------------------------------------------------------------
  quadRule = 3; 
  quad_points.resize(quadRule); 
  quad_weight.resize(quadRule);

  quad_points[0] =  0.0; 
  quad_points[1] = -sqrt(3.0/5.0); 
  quad_points[2] =  sqrt(3.0/5.0); 

  quad_weight[0] = 8.0/9.0; 
  quad_weight[1] = 5.0/9.0;
  quad_weight[2] = 5.0/9.0;*/

  /*
  //----------------------------------------------------------------------
  quadRule = 4; 
  quad_points.resize(quadRule); 
  quad_weight.resize(quadRule);

  quad_points[0] =  sqrt(3.0/7.0 - 2.0/7.0*sqrt(6.0/5.0)); 
  quad_points[1] = -sqrt(3.0/7.0 - 2.0/7.0*sqrt(6.0/5.0)); 
  quad_points[2] =  sqrt(3.0/7.0 + 2.0/7.0*sqrt(6.0/5.0)); 
  quad_points[3] = -sqrt(3.0/7.0 + 2.0/7.0*sqrt(6.0/5.0)); 

  quad_weight[0] = (18.0+sqrt(30.0))/36.0; 
  quad_weight[1] = (18.0+sqrt(30.0))/36.0;
  quad_weight[2] = (18.0-sqrt(30.0))/36.0;
  quad_weight[3] = (18.0-sqrt(30.0))/36.0;*/

  //----------------------------------------------------------------------
  /*
  quadRule = 5; 
  quad_points.resize(quadRule); 
  quad_weight.resize(quadRule);

  quad_points[0] =  0; 
  quad_points[1] =  1.0/3.0*sqrt(5.0-2.0*sqrt(10.0/7.0)); 
  quad_points[2] = -1.0/3.0*sqrt(5.0-2.0*sqrt(10.0/7.0));; 
  quad_points[3] =  1.0/3.0*sqrt(5.0+2.0*sqrt(10.0/7.0));;
  quad_points[4] = -1.0/3.0*sqrt(5.0+2.0*sqrt(10.0/7.0));; 

  quad_weight[0] =  128.0/225.0; 
  quad_weight[1] = (322.0+13.0*sqrt(70.0))/900.0;
  quad_weight[2] = (322.0+13.0*sqrt(70.0))/900.0;
  quad_weight[3] = (322.0-13.0*sqrt(70.0))/900.0;
  quad_weight[4] = (322.0-13.0*sqrt(70.0))/900.0; */

  /*
  //----------------------------------------------------------------------
  quadRule = 15; 
  quad_points.resize(quadRule); quad_weight.resize(quadRule);

  quad_points[0]  =  0; 
  quad_points[1]  = -0.2011940939974345;
  quad_points[2]  =  0.2011940939974345;
  quad_points[3]  = -0.3941513470775634;
  quad_points[4]  =  0.3941513470775634;
  quad_points[5]  = -0.5709721726085388;
  quad_points[6]  =  0.5709721726085388;
  quad_points[7]  = -0.7244177313601701;
  quad_points[8]  =  0.7244177313601701;
  quad_points[9]  = -0.8482065834104272;
  quad_points[10] =  0.8482065834104272;
  quad_points[11] = -0.9372733924007060;
  quad_points[12] =  0.9372733924007060;
  quad_points[13] = -0.9879925180204854;
  quad_points[14] =  0.9879925180204854;

  quad_weight[0]  = 0.2025782419255613; 
  quad_weight[1]  = 0.1984314853271116;	
  quad_weight[2]  = 0.1984314853271116;	
  quad_weight[3]  = 0.1861610000155622;
  quad_weight[4]  = 0.1861610000155622;
  quad_weight[5]  = 0.1662692058169939;
  quad_weight[6]  = 0.1662692058169939;	
  quad_weight[7]  = 0.1395706779261543;	
  quad_weight[8]  = 0.1395706779261543;
  quad_weight[9]  = 0.1071592204671719;
  quad_weight[10] = 0.1071592204671719;
  quad_weight[11] = 0.0703660474881081;	
  quad_weight[12] = 0.0703660474881081;	
  quad_weight[13] = 0.0307532419961173;	
  quad_weight[14] = 0.0307532419961173;*/

  /*
  std::cout << "      Gauss quadrature order      :	" << quadRule << std::endl;
  std::cout << "      Order of basis function     :     " << basisFunctionOrder << std::endl;
  if (basisFunctionOrder <= 2.0*quadRule-1){
  std::cout << "      Compatibility               :     Yes! Order of basis function is compatible with Gauss quadrature order!" << std::endl;
  }
  else 
  {
  std::cout << "      Compatibility               :     No! Please check the order of basis function with Gauss quadrature order!" << std::endl;
  }*/
}