#!/bin/bash
# comments start with a '#"
echo "Running from #!/bin/bash shebang shell..."
# time lapse
SECONDS=0;
sleep 0.5;
# parameter for table of data
sizeTable_E=41;
sizeTable_nu=41;
# parameter for Young E [N/m^2]
E_begin=68.2*10^9;
E_step=2.385*10^9;
# parameter for Poisson nu [-]
nu_begin=0.3;
nu_step=0.004;
# batch script will start here
make
# enter (E,nu) value for the array
for ((i=0; i<sizeTable_E; i++)); do
	# define Young E
	E=$(echo "$E_begin + $i*$E_step" | bc) 
	printf "$E " >> checkEnu.txt
	for ((j=0; j<sizeTable_nu; j++)); do
		# define Poisson nu: fixed step for nu from 0.30 to 0.46
		nu=$(echo "$nu_begin + $j*$nu_step" | bc)
		# make file
		make
		# insert (E,nu)
		./main_LLZO $E $nu 
	done
	printf "\n" >> Senergy.txt
	printf "\n" >> checkEnu.txt
done
printf "Finished executing #!/bin/bash shebang shell after $SECONDS seconds!\n"
